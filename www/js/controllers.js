angular.module('henho.controllers', ['tabSlideBox', 'timer'])

.controller('LoginCtrl', function($scope, $state, $q, UserService, $ionicLoading, $http, Endpoint, signaling) {
  var user = UserService.getUser('facebook');

  if (user.jwt) {
    $state.go('home');
  }

  // This method is to get JWT using facebook auth response
  var getJWT = function(authResponse) {
    getFacebookProfileInfo(authResponse)
      .then(function(profileInfo) {
        fbToken = {
          fb_token: authResponse.accessToken
        };

        $http
          .get(Endpoint.Login, {
            params: fbToken
          })
          .success(function(data, status, headers, config) {
            var jwt = data.token;

            UserService.setUser({
              authResponse: authResponse,
              userID: profileInfo.id,
              name: profileInfo.name,
              email: profileInfo.email,
              jwt: jwt
            });

            $ionicLoading.hide();
            $state.go('home');
          })
          .error(function(data, status, headers, config) {
            // Handle login errors here
            console.log('Error');
          });

      }, function(fail) {
        // Fail get profile info
        console.log('profile info fail', fail);
      });
  };

  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse) {
      fbLoginError('Cannot find the authResponse');
      return;
    }

    getJWT(response.authResponse);
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error) {
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function(authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
      function(response) {
        info.resolve(response);
      },
      function(response) {
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.fbLogin = function() {
    facebookConnectPlugin.getLoginStatus(function(response) {
      console.log('getLoginStatus', response.status);

      if (response.status === 'connected') {
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        // Check if we have our user saved
        var user = UserService.getUser('facebook');

        if (!user.jwt) {
          getJWT(response.authResponse);
        } else {
          $state.go('home');
        }
      } else {
        // If (response.status === 'not_authorized') the user is logged in to Facebook,
        // but has not authenticated your app
        // Else the person is not logged into Facebook,
        // so we're not sure if they are logged into this app or not.
        $ionicLoading.show({
          template: 'Đang đăng nhập...'
        });

        // Ask the permissions you need. You can learn more about
        // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };
})

.controller('HomeCtrl', function($scope, UserService, $ionicActionSheet, $ionicPopup, $state, $ionicLoading, $http, Endpoint, signaling, rooms) {
  $scope.user = UserService.getUser();
  $http.defaults.headers.common['Authorization'] = 'Bearer ' + $scope.user.jwt;

  $scope.retina = window.devicePixelRatio && window.devicePixelRatio >= 1.5; // Check whether this phone is retina
  $scope.data = {};
  $scope.data.name = $scope.user.userID;
  $scope.loading = false;
  $scope.rooms = rooms.get();

  $scope.login = function() {
    $scope.loading = true;
    signaling.emit('login', $scope.user.jwt);
  };
  signaling.on('login_error', function(message) {
    $scope.loading = false;
    var alertPopup = $ionicPopup.alert({
      title: 'Error',
      template: message
    });
  });
  signaling.on('login_successful', function(users) {});
  signaling.on('newRoom', function(roomID, roomName) {
    $scope.rooms.push({
      "id": roomID,
      "name": roomName,
    });
    rooms.set($scope.rooms);
  });

  $scope.logout = function() {
    var hideSheet = $ionicActionSheet.show({
      destructiveText: 'Đăng xuất',
      titleText: 'Bạn có chắc chắn muốn đăng xuất không?',
      cancelText: 'Bỏ qua',
      cancel: function() {},
      buttonClicked: function(index) {
        return true;
      },
      destructiveButtonClicked: function() {
        $ionicLoading.show({
          template: 'Đang đăng xuất...'
        });

        // Set user data to blank
        UserService.setUser({});

        // Facebook logout
        facebookConnectPlugin.logout(function() {
          $ionicLoading.hide();
          $state.go('login');
        }, function(fail) {
          $ionicLoading.hide();
        });
      }
    });
  };
})

.controller('ChatCtrl', function($scope, $state, $stateParams) {
  $scope.room = $stateParams.room;
  $scope.goContacts = function() {
    $state.go('home');
  };
});