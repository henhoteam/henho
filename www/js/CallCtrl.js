angular.module('henho')

.controller('CallCtrl', function($scope, $ionicActionSheet, $state, signaling) {
  $scope.callInProgress = true;
  $scope.callFinished = false;
  $scope.talkingTime = 3;
  $scope.decidingTime = 10;
  $scope.moreSheet = function() {};

  // Notice the server this user is calling
  signaling.emit('call');

  function call(isInitiator) {
    console.log(new Date().toString() + ': calling ' + ', isInitiator: ' + isInitiator);

    var config = {
      isInitiator: isInitiator,
      turn: {
        host: 'turn:stun-turn.org:3478',
        username: '',
        password: ''
      },
      streams: {
        audio: true,
        video: false
      }
    };

    var session = new cordova.plugins.phonertc.Session(config);

    session.on('sendMessage', function(data) {
      signaling.emit('sendMessage', {
        type: 'phonertc_handshake',
        data: JSON.stringify(data)
      });
    });

    session.on('answer', function() {
      console.log('Answered!');
    });

    session.on('disconnect', function() {});

    session.call();

    $scope.session = session;
  };

  function onMessageReceive(message) {
    console.log(message);
    switch (message.type) {
      case 'call':
        call(false);
        signaling.emit('sendMessage', {
          type: 'answer'
        });
        $scope.callInProgress = true;
        break;
      case 'answer':
        call(true);
        $scope.callInProgress = true;
        break;
      case 'phonertc_handshake':
        $scope.session.receiveMessage(JSON.parse(message.data));
        break;
    }
  };
  signaling.on('messageReceived', onMessageReceive);

  signaling.emit('sendMessage', {
    type: 'call'
  });

  function report() {

  }

  function endCall() {
    $scope.back();
  }

  $scope.more = function() {
    $scope.moreSheet = $ionicActionSheet.show({
      buttons: [{
        text: 'Báo cáo xấu'
      }, {
        text: 'Kết thúc cuộc gọi'
      }],
      cancelText: 'Bỏ qua',
      cancel: function() {},
      buttonClicked: function(index) {
        switch (index) {
          case 0:
            report();
            break;
          case 1:
            endCall();
            break;
          default:
        }
        return true;
      },
    });
  };

  $scope.back = function() {
    $state.go('home');
  };

  $scope.finish = function() {
    $scope.$apply(function() {
      $scope.moreSheet(); // Cancel more action sheet
      $scope.callInProgress = !$scope.callInProgress;
      $scope.callFinished = true;
    });
  };

  $scope.like = function() {

  };
});