angular.module('henho')
  .factory('rooms', function() {
    var set = function(rooms) {
      var db = JSON.parse(window.localStorage.db || '{"rooms": []}');
      db['rooms'] = rooms;
      window.localStorage.db = JSON.stringify(db);
    };

    // Get returns an array of rooms
    var get = function() {
      var db = JSON.parse(window.localStorage.db || '{"rooms": []}');
      return [{
        "id": "123",
        "name": "test"
      }, {
        "id": "124",
        "name": "Cỏ"
      }, {
        "id": "124",
        "name": "Cỏ"
      }, {
        "id": "124",
        "name": "Cỏ"
      }, {
        "id": "124",
        "name": "Cỏ"
      }, {
        "id": "124",
        "name": "Cỏ"
      }, {
        "id": "124",
        "name": "Cỏ"
      }, {
        "id": "124",
        "name": "Cỏ"
      }, {
        "id": "124",
        "name": "Cỏ"
      }, {
        "id": "124",
        "name": "Cỏ"
      }, {
        "id": "124",
        "name": "Cỏ"
      }];
      // return db['rooms'];
    };

    return {
      get: get,
      set: set,
    };
  });