// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('henho', ['ionic', 'henho.controllers', 'henho.services', 'btford.socket-io', 'ngCordova'])

.constant('Endpoint', {
  'Login': 'http://192.168.1.2:3001/login',
})

.run(function($ionicPlatform, $cordovaSQLite, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }

    ionic.Platform.fullScreen(true, true);
    if (window.StatusBar) {
      StatusBar.styleDefault();
      StatusBar.overlaysWebView(false);
    }

    // Initialize database
    $rootScope.db = $cordovaSQLite.openDB({
      name: 'my.db',
      location: 'default'
    });
  });
})

.config(function($httpProvider, $stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  // Disable swipe back to enable long duration for button
  $ionicConfigProvider.views.swipeBackEnabled(false);

  // Set transition style by platform
  $ionicConfigProvider.views.transition('platform');

  $stateProvider

  // Login state
    .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  })

  // Home state
  .state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: 'HomeCtrl'
  })

  // Call state
  .state('call', {
    url: '/call',
    templateUrl: 'templates/call.html',
    controller: 'CallCtrl'
  })

  // Call state
  .state('chat', {
    url: '/chat',
    params: {
      room: null
    },
    templateUrl: 'templates/chat.html',
    controller: 'ChatCtrl'
  });

  $urlRouterProvider.otherwise('/login');

});