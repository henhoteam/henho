angular.module('henho')
  .factory('signaling', function(socketFactory) {
    var socket = io('http://192.168.0.105:3000/');

    var socketFactory = socketFactory({
      ioSocket: socket
    });

    return socketFactory;
  });